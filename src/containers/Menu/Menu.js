import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrder, addProductToCard} from "../../store/actions";
import './Menu.css';
import cart from '../../assets/shopping-cart.png';

const Menu = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.menuItems.dishes);

    useEffect(() => {
        dispatch(fetchOrder())
    }, [dispatch]);

    return (
        <div>
            {dishes.map((dish, i) => (
                <div key={i} className="Item" onClick={() => dispatch(addProductToCard(dish))}>
                    <img src={dish.image} alt='itemImg' width='70px' className="click"/>
                    <p>{dish.name}</p>
                    <p>{dish.price} KGS</p>
                    <button className="btn"><img src={cart} alt="add to cart" className="click" width='30px' /></button>
                </div>
                )
            )}
        </div>
    );
};

export default Menu;