import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Cart.css';
import {removeProductFromCard} from "../../store/actions";
import deleteIcon from '../../assets/delete.png';
import Modal from "../../components/UI/Modal/Modal";
import {createOrder as order} from '../../store/actions';
import Spinner from "../../components/UI/Spinner/Spinner";

const Cart = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.cartItems.orders);
    const totalSum = useSelector(state => state.cartItems.totalSum);
    const loading = useSelector(state => state.cartItems.loading);

    const [open, setOpen] = useState(false);
    const [customer, setCustomer] = useState({
        name: '',
        street: '',
        phone: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setCustomer(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const purchaseHandler = () => {
        setOpen(!open);
    };

    const createOrder = async e => {
        e.preventDefault();
        await dispatch(order({customer, orders}));
    };

    let contactData = (
        <form onSubmit={createOrder}>
            <input
                className="Input"
                type="text"
                name="name"
                placeholder="Your Name"
                value={customer.name}
                onChange={onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="phone"
                placeholder="Your Phone"
                value={customer.phone}
                onChange={onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="street"
                placeholder="Street"
                value={customer.street}
                onChange={onInputChange}
            />
            <button className="OrderButton">ORDER</button>
        </form>
    )

    if(loading) {
        contactData = <Spinner/>
    }

    return (
        <>
            <Modal
                show={open}
                closed={purchaseHandler}
            >
                <div className="ContactData">
                    <h4>Enter your contact data</h4>
                    {Object.keys(orders).map((product, i) => (
                        <div key={i} className="OrderList">
                            <p>{product}</p>
                            <p>x{orders[product].amount}</p>
                            <p>{orders[product].amount * orders[product].price} KGS</p>
                        </div>
                    ))}
                    <p>Total : {totalSum} KGS</p>

                </div>
                {contactData}

            </Modal>
            <div>
                {Object.keys(orders).map((product, i) => (
                    <div key={i} className="Order">
                        <p>{product}</p>
                        <p>x{orders[product].amount}</p>
                        <p>{orders[product].amount * orders[product].price} KGS</p>
                        <button onClick={() => dispatch(removeProductFromCard({name: product, amount: orders[product].amount}))} className="btn"><img src={deleteIcon} alt='deleteIcon' width='30px' className="click"/></button>
                    </div>
                ))}
                <div className="Total">
                    <p>Доставка: <strong>150 KGS</strong></p>
                    <p>Итого: <strong>{totalSum} KGS</strong></p>
                    <button disabled={totalSum <= 150} onClick={purchaseHandler} className="OrderButton">Place Order</button>
                </div>
            </div>
        </>

    );
};

export default Cart;