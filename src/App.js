import './App.css';
import Menu from "./containers/Menu/Menu";
import Cart from "./containers/Cart/Cart";

const App = () => (
   <div className="Container">
       <div className="Block MenuBlock">
           <h2>Menu list</h2>
           <div className="Menu">
               <Menu/>
           </div>
       </div>
       <div className="Block Cart">
           <h2>Cart</h2>
           <div className="Cart">
               <Cart/>
           </div>
       </div>

   </div>
);

export default App;
