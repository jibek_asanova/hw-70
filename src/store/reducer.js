import {
    FETCH_ORDER_FAILURE,
    FETCH_ORDER_REQUEST,
    FETCH_ORDER_SUCCESS
} from "./actions";

const initialState = {
    error: null,
    dishes: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_ORDER_REQUEST:
            return {...state, error: null, loading: true};
        case FETCH_ORDER_SUCCESS:
            const dishes = [];
            Object.keys(action.payload).forEach((key, index) => {
                dishes.push({
                    id: index,
                    name: key,
                    count: 0,
                    ...action.payload[key],
                })
            });

            return {...state, loading: false, dishes};
        case FETCH_ORDER_FAILURE:
            return {...state, loading: false, error: action.payload};


        default:
            return state;
    }
}

export default reducer;