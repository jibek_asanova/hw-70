import {
    ADD_PRODUCT_TO_CARD, CREATE_ORDER_FAILURE,
    CREATE_ORDER_REQUEST,
    CREATE_ORDER_SUCCESS,
    REMOVE_PRODUCT_FROM_CARD
} from "./actions";

const initialState = {
    orders: {},
    totalSum: 150,
    purchasing: false,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case ADD_PRODUCT_TO_CARD:
            let orders = {...state.orders};
            let totalSum = state.totalSum;
            if(!orders[action.payload.name]) {
                orders[action.payload.name] = {amount: 1, price: action.payload.price};
            } else {
                orders[action.payload.name].amount++;
            }
            totalSum += orders[action.payload.name].price;


            return {
                ...state,
                orders, totalSum
            };
        case REMOVE_PRODUCT_FROM_CARD:
            let removeOrder = {...state.orders};
            let totalSumR = state.totalSum;

            if (removeOrder[action.payload.name].amount > 0) {
                removeOrder[action.payload.name].amount--;
                totalSumR -= removeOrder[action.payload.name].price;

                if(removeOrder[action.payload.name].amount === 0) {
                    delete removeOrder[action.payload.name];
                }}


            return {...state, orders: removeOrder, totalSum: totalSumR}
        case CREATE_ORDER_REQUEST:
            return {...state, error: null, loading: true};
        case CREATE_ORDER_SUCCESS:
            return {...state, loading: false};
        case CREATE_ORDER_FAILURE:
            return {...state, loading: false, error: action.payload}
        default:
            return state;
    }
}

export default reducer;