import axios from "axios";

export const FETCH_ORDER_REQUEST = 'FETCH_ORDER_REQUEST';
export const FETCH_ORDER_SUCCESS = 'FETCH_ORDER_SUCCESS';
export const FETCH_ORDER_FAILURE = 'FETCH_ORDER_FAILURE';
export const ADD_PRODUCT_TO_CARD = 'ADD_PRODUCT_TO_CARD';
export const REMOVE_PRODUCT_FROM_CARD = 'REMOVE_PRODUCT_FROM_CARD';
export const CREATE_ORDER_REQUEST = 'CREATE_ORDER_REQUEST';
export const CREATE_ORDER_SUCCESS = 'CREATE_ORDER_SUCCESS';
export const CREATE_ORDER_FAILURE = 'CREATE_ORDER_FAILURE';


export const fetchOrderRequest = () => ({type: FETCH_ORDER_REQUEST});
export const fetchOrderSuccess = orders => ({type: FETCH_ORDER_SUCCESS, payload: orders});
export const fetchOrderFailure = () => ({type: FETCH_ORDER_FAILURE});
export const addProductToCard = order => ({type: ADD_PRODUCT_TO_CARD, payload: order});
export const removeProductFromCard = order => ({type: REMOVE_PRODUCT_FROM_CARD, payload: order});
export const createOrderRequest = () => ({type: CREATE_ORDER_REQUEST});
export const createOrderSuccess = orders => ({type: CREATE_ORDER_SUCCESS, payload: orders});
export const createOrderFailure = () => ({type: CREATE_ORDER_FAILURE});


export const fetchOrder = () => {
    return async (dispatch) => {
        dispatch(fetchOrderRequest());
        try {
            const response = await axios.get('https://jibek-asanova-default-rtdb.firebaseio.com/products.json');
            const orders = response.data;
            dispatch(fetchOrderSuccess(orders));
        } catch (e) {
            dispatch(fetchOrderFailure())
        }
    }
}

export const createOrder = orderData => {
    return async dispatch => {
        try {
            dispatch(createOrderRequest())
            await axios.post('https://jibek-asanova-default-rtdb.firebaseio.com/orders.json', orderData);
            dispatch(createOrderSuccess());
        } catch (error) {
            dispatch(createOrderFailure(error));
            throw error;
        }
    }
}

